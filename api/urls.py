from django.conf.urls import url, include
from django.urls import path
from .views import CreateUserAPIView, ListUserAPIView, LoginView, VenteView, DepenseView, PretView, EntreeView, SortieView, HistoriqueView
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token
from api import views
from api.views import ClientList, EmployeList, FournisseurList, LoginEmploye, ProduitView
routers = routers.DefaultRouter()

app_name = 'api'                    

urlpatterns = [
    url(r'^registeruser/', CreateUserAPIView.as_view()),
    url(r'^listuser/', ListUserAPIView.as_view()),
    url(r'^loginuser/', LoginView.as_view(), name="login"),
    url(r'^userupdate/(?P<pk>[0-9]+)/', views.UserUpdateView.as_view()),
    url(r'^depense/(?P<pk>[0-9]+)/', DepenseView.as_view(), name="depense"),
    url(r'^depenseupdate/(?P<pk>[0-9]+)/', views.DepenseUpdateView.as_view()),
    url(r'^depensedelete/(?P<pk>[0-9]+)/', views.DepenseDeleteView.as_view()),
    url(r'^vente/(?P<pk>[0-9]+)/', VenteView.as_view(), name="vente"),
    url(r'^venteupdate/(?P<pk>[0-9]+)/', views.VenteUpdateView.as_view()),
    url(r'^ventedelete/(?P<pk>[0-9]+)/', views.VenteDeleteView.as_view()),
    url(r'^pret/(?P<pk>[0-9]+)/', PretView.as_view(), name="pret"),
    url(r'^pretupdate/(?P<pk>[0-9]+)/', views.PretUpdateView.as_view()),
    url(r'^pretdelete/(?P<pk>[0-9]+)/', views.PretDeleteView.as_view()),
    url(r'^produit/(?P<pk>[0-9]+)', ProduitView.as_view(), name="produit"),
    url(r'^produitupdate/(?P<pk>[0-9]+)/', views.ProduitUpdateView.as_view()),
    url(r'^produitdelete/(?P<pk>[0-9]+)/', views.ProduitDeleteView.as_view()),
    url(r'^Entree/(?P<pk>[0-9]+)/', EntreeView.as_view(), name="Gestion de Stock"),
    url(r'^updateentree/(?P<pk>[0-9]+)/', views.EntreeUpdateView.as_view()),
    url(r'^deleteentree/(?P<pk>[0-9]+)/', views.EntreeDeleteView.as_view()),
    url(r'^Sortie/(?P<pk>[0-9]+)/', SortieView.as_view(), name="Gestion de Stock"),
    url(r'^updatesortie/(?P<pk>[0-9]+)/', views.SortieUpdateView.as_view()),
    url(r'^deletesortie/(?P<pk>[0-9]+)/', views.SortieDeleteView.as_view()),
    url(r'^Historique/(?P<pk>[0-9]+)/', HistoriqueView.as_view(), name="Gestion de Stock"),
    url(r'^client/(?P<pk>[0-9]+)', views.ClientList.as_view(), name="client"),
    url(r'^updateclient/(?P<pk>[0-9]+)/', views.ClientUpdateView.as_view()),
    url(r'^deleteclient/(?P<pk>[0-9]+)/', views.ClientDeleteView.as_view()),
    url(r'^fournisseur/(?P<pk>[0-9]+)', views.FournisseurList.as_view(), name="fournisseur"),
    url(r'^updatefournisseur/(?P<pk>[0-9]+)/', views.FournisseurUpdateView.as_view()),
    url(r'^deletefournisseur/(?P<pk>[0-9]+)/', views.FournisseurDeleteView.as_view()),
    url(r'^commande/(?P<pk>[0-9]+)',
        views.CommandeList.as_view(), name="commande"),
    url(r'^updatecommande/(?P<pk>[0-9]+)/',
        views.CommandeUpdateView.as_view()),
    url(r'^deletecommande/(?P<pk>[0-9]+)/',
        views.CommandeDeleteView.as_view()),
    url(r'^boutique/(?P<pk>[0-9]+)/',
         views.BoutiqueList.as_view(), name="boutique"),
    url(r'^updateboutique/(?P<pk>[0-9]+)/',
        views.BoutiqueUpdateView.as_view()),
    url(r'^deleteboutique/(?P<pk>[0-9]+)/',
        views.BoutiqueDeleteView.as_view()),
    url(r'^employe/(?P<pk>[0-9]+)/',
        EmployeList.as_view(), name="Employe"),
    url(r'^updateemploye/(?P<pk>[0-9]+)/',
        views.EmployeUpdateView.as_view()),
    url(r'^deleteemploye/(?P<pk>[0-9]+)/',
        views.EmployeDeleteView.as_view()),
    url(r'^loginemploye/', LoginEmploye.as_view(), name="loginEmploye"),
]

urlpatterns += [
    path('api-auth/', include('rest_framework.urls')),
]
