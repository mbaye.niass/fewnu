from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from api.models import Boutique, Client, Commande, Depense, Employe, Entree, Fournisseur, Historique, Pret, Produit, Sortie, User, Vente


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('phone', 'nom_complet')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('phone', 'password', 'nom_complet', 'is_active')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class EmployeInline(admin.StackedInline):
    model = Employe
    can_delete = False


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('phone', 'nom_complet')
    #list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('phone', 'password')}),
        ('Personal info', {'fields': ('nom_complet',)}),
        (('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')})
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('phone', 'nom_complet', 'password1', 'password2')}
        ),
    )
    list_display = ('phone', 'nom_complet', 'is_staff', 'is_superuser')
    search_fields = ('phone',)
    ordering = ('phone',)
    filter_horizontal = ()
    inlines = (EmployeInline, )

# Now register the new UserAdmin...
admin.site.register(User, UserAdmin)
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)
admin.site.register(Vente)
admin.site.register(Depense)
admin.site.register(Pret)
admin.site.register(Entree)
admin.site.register(Sortie)
admin.site.register(Historique)
admin.site.register(Produit)
admin.site.register(Client)
admin.site.register(Fournisseur)
admin.site.register(Boutique)
admin.site.register(Commande)
admin.site.register(Employe)
""" @admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    pass """

""" class EntreeInline(admin.TabularInline):
    model = Entree.fournisseur.through
    extra = 1


@admin.register(Fournisseur)
class FournisseurAdmin(admin.ModelAdmin):
    inlines = [EntreeInline, ]  # list of Fournisseurs made by a Entree
 """
    
"""class EntreInline(admin.TabularInline):
    model = Entree.produit.through
    extra = 1

@admin.register(Produit)
class ProduitAdmin(admin.ModelAdmin):
    inlines = [EntreInline, ]"""
