from __future__ import unicode_literals
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from django.db import transaction
from django.contrib.contenttypes.fields import GenericForeignKey
from api_fewnu import settings
from rest_framework import authentication, exceptions
import jwt


class UserManager(BaseUserManager):
    def _create_user(self, phone, nom_complet, password, **extra_fields):
        """
        Creates and saves a User with the given phone, nom_complet and password.
         """
        if not phone and not nom_complet:
            raise ValueError(
                'Users must have a phone number and a nom_complet')
        try:
            with transaction.atomic():
                user = self.model(
                    phone=phone, nom_complet=nom_complet, **extra_fields)
                user.set_password(password)
                user.save(using=self._db)
                return user
        except:
            raise

    def create_user(self, phone, nom_complet, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(phone, nom_complet, password=password, **extra_fields)

    def create_superuser(self, phone, nom_complet, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        return self._create_user(phone, nom_complet, password=password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.
 
    """
    phone = models.CharField(max_length=40, unique=True)
    nom_complet = models.CharField(max_length=30, blank=True)
    email = models.EmailField(("Email"), max_length=254, blank=True, null=True)
    adresse = models.CharField(blank=True, max_length=255, null=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['nom_complet']

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self


class Client(models.Model):
    Date = models.DateTimeField(default=timezone.now)
    Nom_Complet = models.CharField(max_length=30)
    Telephone = models.CharField(max_length=30)
    Adresse = models.CharField(max_length=30)
    id_user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.Nom_Complet)


class Depense(models.Model):
    Date = models.DateTimeField(default=timezone.now)
    Designation = models.CharField(max_length=30)
    Prix = models.IntegerField(('prix'), default=10)
    id_userd = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {}".format(self.Designation, self.Prix)


class Pret(models.Model):
    Date = models.DateTimeField(default=timezone.now)
    Telephone = models.CharField(max_length=30)
    Designation = models.CharField(max_length=30)
    Montant = models.IntegerField(('prix'), default=10)
    Client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,)
    id_userp = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {}".format(self.Client.Nom_Complet, self.Montant)


class Fournisseur(models.Model):
    Date = models.DateTimeField(default=timezone.now)
    Nom_Complet = models.CharField(max_length=30)
    Telephone = models.CharField(max_length=30)
    Adresse = models.CharField(max_length=30)
    id_user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.Nom_Complet)


class Gestion_de_Stock(models.Model):
    Prix_Unitaire = models.IntegerField(('prix'), default=10)
    Quantite = models.IntegerField(('quantite'))
    id_users = models.ForeignKey(
        User, default="", on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Produit(models.Model):
    Date = models.DateTimeField(default=timezone.now)
    Produit = models.CharField(max_length=30)
    Prix = models.IntegerField(('prix'))
    Quantite = models.IntegerField(('quantite'))
    file = models.FileField(blank=True, null=True)
    id_userv = models.ForeignKey(User, on_delete=models.CASCADE)
    

    def __str__(self):
        return "{} ".format(self.Produit)


class Entree(Gestion_de_Stock):
    Produit = models.ForeignKey(
        Produit, related_name='entree',
        on_delete=models.CASCADE,)
    type = models.CharField(max_length=30, default="Entree")
    fournisseur = models.ForeignKey(
        Fournisseur,
        on_delete=models.CASCADE,)

    def save(self, *args, **kwargs):
        p = self.Produit
        p.Quantite += self.Quantite
        if p.Prix <= self.Prix_Unitaire:
            p.Prix = self.Prix_Unitaire
        p.save()
        super(Entree, self).save(*args, **kwargs)

    def __str__(self):
        return "{}".format(self.Produit.Produit)


class Sortie(Gestion_de_Stock):
    Produit = models.ForeignKey(
        Produit, related_name='sortie',
        on_delete=models.CASCADE,)
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,)
    type = models.CharField(max_length=30, default="Sortie")

    def __str__(self):
        return "{}".format(self.Produit.Produit)
                
    def save(self, *args, **kwargs):
        p = self.Produit
        p.Quantite -= self.Quantite
        p.save()
        super(Sortie, self).save(*args, **kwargs)


class Vente(models.Model):
    Date = models.DateTimeField(default=timezone.now)
    #Designation = models.CharField(max_length=30)
    Designation = models.ForeignKey(
        Produit, related_name='vente',
        on_delete=models.CASCADE,)
    Quantite = models.IntegerField(('Quantite vendu'))
    Prix = models.IntegerField(('prix'), default=1)
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,)
    id_userv = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.Designation.Produit)

    def save(self, *args, **kwargs):
        p = self.Designation
        p.Quantite -= self.Quantite
        p.save()
        super(Vente, self).save(*args, **kwargs)


class Historique(Gestion_de_Stock):
    type = models.CharField(max_length=30, default="")


class Boutique(models.Model):
    Nom_Gerant = models.CharField(max_length=100, blank = True)
    Nom_Boutique = models.CharField(max_length = 100, blank = True)
    Telephone = models.CharField(max_length = 14, blank = False, unique = True)
    Email = models.EmailField(blank = True)
    Adresse = models.CharField(max_length = 30, blank = True)
    id_user = models.ForeignKey(User, related_name='boutique', on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.Nom_Boutique)


class Commande(models.Model):
    Produit = models.ForeignKey(
        Produit, related_name='order',
        on_delete=models.CASCADE,)
    Quantite = models.IntegerField(('quantite'))
    Client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,)
    id_user = models.ForeignKey(User, on_delete=models.CASCADE)
    
    def __str__(self):
        return "{}".format(self.Produit)

    def save(self, *args, **kwargs):
        p = self.Produit
        p.Quantite -= self.Quantite
        p.save()
        super(Commande, self).save(*args, **kwargs)


class Employe(User, PermissionsMixin):
    salaire = models.IntegerField(("Salaire"), blank=True, null=True)

    def __str__(self):
        return self.nom_complet
    
    def save(self, *args, **kwargs):
        u = self.user_emp_id
        u = self.id
        print(u)
        super(Employe, self).save(
            *args, **kwargs
        ) 
