from .permissions import CustomDjangoModelPermissions
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from api.models import Boutique, Client, Commande, Depense, Employe, Entree, Fournisseur, Historique, Pret, Produit, Sortie, User, Vente
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated, SAFE_METHODS, BasePermission
from rest_framework.response import Response
from rest_framework import response
from rest_framework.views import APIView
from .serializers import UserSerializer, LoginSerializer, TokenSerializer, VenteSerializer, DepenseSerializer, PretSerializer, ProduitSerializer, EntreeSerializer, SortieSerializer, HistoriqueSerializer
from rest_framework import status
from rest_framework_jwt.settings import api_settings
import jwt
from rest_framework import permissions
from django.contrib.auth import authenticate, login
from rest_framework import generics
from api_fewnu import settings
from django.contrib.auth.signals import user_logged_in
from api.serializers import BoutiqueSerializer, ClientSerializer, CommandeSerializer, EmployeSerializer, FournisseurSerializer
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

# Create your views here.


class CreateUserAPIView(APIView):
    # Allow any user (authenticated or not) to access this url
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer

    def post(self, request):
        user = request.data
        serializer = UserSerializer(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ListUserAPIView(APIView):
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        user = User.objects.all()

        if not user:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = UserSerializer(user, many=True)
        print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "user successfully retrieved.",
            "count": user.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class LoginView(generics.CreateAPIView):
    """
    POST auth/login/
    """

    # This permission class will over ride the global permission
    # class setting
    permission_classes = (permissions.AllowAny,)

    queryset = User.objects.all()
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        phone = request.data.get("phone", "")
        password = request.data.get("password", "")
        user = authenticate(request, phone=phone, password=password)
        if user is not None:
            # login saves the user’s ID in the session,
            # using Django’s session framework.
            login(request, user)

            serializer = TokenSerializer(data={
                # using drf jwt utility functions to generate a token
                "token": jwt_encode_handler(
                    jwt_payload_handler(user)
                )
            })
            if serializer.is_valid():
                user = request.user
                token = serializer.data
                response_data = {
                    'token': token,
                    'id': user.id,
                    'nom_complet': user.nom_complet,
                    'phone': user.phone
                }
                return Response(response_data)
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class UserUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def put(self, request, *args, **kwargs):
        try:
            user = User.objects.get(id=kwargs['pk'])
        except user.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        user_data = {
            'nom_complet': request.data['nom_complet'],
            'phone': request.data['phone'],
            'Email': request.data['Email'],
            'Adresse': request.data['Adresse']
        }

        serializer = UserSerializer(user, data=user_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "errors": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "user successfully updated",
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class DepenseView(generics.CreateAPIView):
    serializer_class = DepenseSerializer

    def get(self, request, *args, **kwargs):
        depense = Depense.objects.filter(id_userd=self.kwargs['pk'])
        if not depense:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = DepenseSerializer(depense, many=True)
        #print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "item successfully retrieved.",
            "count": depense.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = DepenseSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully created",
        }, status=status.HTTP_201_CREATED)


class DepenseUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Depense.objects.all()
    serializer_class = DepenseSerializer

    def put(self, request, *args, **kwargs):
        try:
            depense = Depense.objects.get(id=kwargs['pk'])
        except Depense.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        depense_data = {
            'Designation': request.data['Designation'],
            'Prix': request.data['Prix'],
            'id_userd': request.data['id_userd'],
            'Date': request.data['Date'],
        }

        serializer = DepenseSerializer(
            depense, data=depense_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "errors": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully update",
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class DepenseDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Depense.objects.all()
    serializer_class = DepenseSerializer

    def post(self, request, *args, **kwargs):
        try:
            Depense = Depense.objects.get(id=kwargs['pk'])
        except Depense.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        Depense.delete()
        return Response({
            "status": "succesc",
            "message": "depense successfully deleted"
        }, status=status.HTTP_200_OK)


class VenteView(generics.CreateAPIView):
    serializer_class = VenteSerializer

    def get(self, request, *args, **kwargs):
        vente = Vente.objects.filter(id_userv=self.kwargs['pk'])
        if not vente:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = VenteSerializer(vente, many=True)
        #print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "item successfully retrieved.",
            "count": vente.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = VenteSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully create",
        }, status=status.HTTP_201_CREATED)


class VenteUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Vente.objects.all()
    serializer_class = VenteSerializer

    def put(self, request, *args, **kwargs):
        try:
            vente = Vente.objects.get(id=kwargs['pk'])
        except Vente.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        vente_data = {
            'Designation': request.data['Designation'],
            'Prix_Unitaire': request.data['Prix_Unitaire'],
            'id_userv': request.data['id_userv'],
            'Date': request.data['Date'],
        }

        serializer = VenteSerializer(vente, data=vente_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "errors": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully update",
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class VenteDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Vente.objects.all()
    serializer_class = VenteSerializer

    def post(self, request, *args, **kwargs):
        try:
            vente = Vente.objects.get(id=kwargs['pk'])
        except Vente.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        vente.delete()
        return Response({
            "status": "succes",
            "message": "vente successfully deleted"
        }, status=status.HTTP_200_OK)


class PretView(generics.CreateAPIView):
    serializer_class = PretSerializer

    def get(self, request, *args, **kwargs):
        pret = Pret.objects.filter(id_userp=self.kwargs['pk'])

        if not pret:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = PretSerializer(pret, many=True)
        #print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "item successfully retrieved.",
            "count": pret.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = PretSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully create",
        }, status=status.HTTP_201_CREATED)

class PretUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Pret.objects.all()
    serializer_class = PretSerializer

    def put(self, request, *args, **kwargs):
        try:
            pret = Pret.objects.get(id=kwargs['pk'])
        except Pret.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        pret_data = {
            'Designation': request.data['Designation'],
            'Montant': request.data['Montant'],
            'id_userp': request.data['id_userp'],
            'Date': request.data['Date'],
            'Client': request.data['Client'],
            'Telephone': request.data['Telephone'],
        }

        serializer = PretSerializer(pret, data=pret_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "errors": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully update",
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class PretDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Pret.objects.all()
    serializer_class = PretSerializer

    def post(self, request, *args, **kwargs):
        try:
            pret = Pret.objects.get(id=kwargs['pk'])
        except Pret.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        pret.delete()
        return Response({
            "status": "succesc",
            "message": "pret successfully deleted"
        }, status=status.HTTP_200_OK)


class ProduitView(generics.CreateAPIView):
    serializer_class = ProduitSerializer

    def get(self, request, *args, **kwargs):
        produit = Produit.objects.filter(id_userv=self.kwargs['pk'])
        if not produit:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = ProduitSerializer(produit, many=True)
        #print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "item successfully retrieved.",
            "count": produit.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = ProduitSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully create",
        }, status=status.HTTP_201_CREATED)


class ProduitUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Produit.objects.all()
    serializer_class = ProduitSerializer

    def put(self, request, *args, **kwargs):
        try:
            produit = Produit.objects.get(id=kwargs['pk'])
        except Produit.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        produit_data = {
            'Designation': request.data['Designation'],
            'Prix': request.data['Prix'],
            'Quantite': request.data['Quantite'],
            'Date': request.data['Date'],
        }

        serializer = ProduitSerializer(
            Produit, data=produit_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "errors": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully update",
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class ProduitDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Produit.objects.all()
    serializer_class = ProduitSerializer

    def post(self, request, *args, **kwargs):
        try:
            Produit = Produit.objects.get(id=kwargs['pk'])
        except Produit.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        Produit.delete()
        return Response({
            "status": "succesc",
            "message": "depense successfully deleted"
        }, status=status.HTTP_200_OK)


class EntreeView(generics.CreateAPIView):
    serializer_class = EntreeSerializer

    def get(self, request, *args, **kwargs):
        entree = Entree.objects.filter(id_users=self.kwargs['pk'])

        if not entree:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = EntreeSerializer(entree, many=True)
        #print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "item successfully retrieved.",
            "count": entree.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = EntreeSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully create",
        }, status=status.HTTP_201_CREATED)


class EntreeUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Entree.objects.all()
    serializer_class = EntreeSerializer

    def put(self, request, *args, **kwargs):
        try:
            entree = Entree.objects.get(id=kwargs['pk'])
        except Entree.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        entree_data = {
            'Produit': request.data['Produit'],
            'Prix_Unitaire': request.data['Prix_Unitaire'],
            'Quantite': request.data['Quantite'],
            'id_users': request.data['id_users'],
            'Date': request.data['Date'],
        }

        serializer = EntreeSerializer(Entree, data=entree_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "errors": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully update",
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class EntreeDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Entree.objects.all()
    serializer_class = EntreeSerializer

    def post(self, request, *args, **kwargs):
        try:
            Entree = Entree.objects.get(id=kwargs['pk'])
        except Entree.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        Entree.delete()
        return Response({
            "status": "succesc",
            "message": "depense successfully deleted"
        }, status=status.HTTP_200_OK)


class SortieView(generics.CreateAPIView):
    serializer_class = SortieSerializer

    def get(self, request, *args, **kwargs):
        sortie = Sortie.objects.filter(id_users=self.kwargs['pk'])

        if not sortie:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = SortieSerializer(sortie, many=True)
        #print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "item successfully retrieved.",
            "count": sortie.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = SortieSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully create",
        }, status=status.HTTP_201_CREATED)


class SortieUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Sortie.objects.all()
    serializer_class = SortieSerializer

    def put(self, request, *args, **kwargs):
        try:
            sortie = Sortie.objects.get(id=kwargs['pk'])
        except Sortie.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        sortie_data = {
            'Produit': request.data['Produit'],
            'Prix_Unitaire': request.data['Prix_Unitaire'],
            'Quantite': request.data['Quantite'],
            'id_users': request.data['id_users'],
            'Date': request.data['Date'],
        }

        serializer = SortieSerializer(Sortie, data=sortie_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "errors": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully update",
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class SortieDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Sortie.objects.all()
    serializer_class = SortieSerializer

    def post(self, request, *args, **kwargs):
        try:
            Sortie = Sortie.objects.get(id=kwargs['pk'])
        except Sortie.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        Sortie.delete()
        return Response({
            "status": "succesc",
            "message": "depense successfully deleted"
        }, status=status.HTTP_200_OK)


class HistoriqueView(generics.CreateAPIView):
    serializer_class = HistoriqueSerializer

    def get(self, request, *args, **kwargs):
        historique = Historique.objects.filter(id_users=self.kwargs['pk'])

        if not historique:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = HistoriqueSerializer(historique, many=True)
        #print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "item successfully retrieved.",
            "count": historique.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = HistoriqueSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully create",
        }, status=status.HTTP_201_CREATED)


class ClientList(generics.CreateAPIView):
    serializer_class = ClientSerializer

    def get(self, request, *args, **kwargs):
        client = Client.objects.filter(id_user=self.kwargs['pk'])

        if not client:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = ClientSerializer(client, many=True)
        #print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "item successfully retrieved.",
            "count": client.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = ClientSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully create",
        }, status=status.HTTP_201_CREATED)


class ClientUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def put(self, request, *args, **kwargs):
        try:
            client = Client.objects.get(id=kwargs['pk'])
        except Client.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        client_data = {
            'Nom_Complet': request.data['Nom_Complet'],
            'Telephone': request.data['Telephone'],
            'Adresse': request.data['Adresse'],
            'id_user': request.data['id_user'],
            'Date': request.data['Date'],
        }

        serializer = ClientSerializer(Client, data=client_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "errors": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully update",
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class ClientDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def post(self, request, *args, **kwargs):
        try:
            Client = Client.objects.get(id=kwargs['pk'])
        except Client.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        Client.delete()
        return Response({
            "status": "succesc",
            "message": "depense successfully deleted"
        }, status=status.HTTP_200_OK)


class FournisseurList(generics.CreateAPIView):
    serializer_class = FournisseurSerializer

    def get(self, request, *args, **kwargs):
        fournisseur = Fournisseur.objects.filter(id_user=self.kwargs['pk'])

        if not fournisseur:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = FournisseurSerializer(fournisseur, many=True)
        #print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "item successfully retrieved.",
            "count": fournisseur.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = FournisseurSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully create",
        }, status=status.HTTP_201_CREATED)


class FournisseurUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Fournisseur.objects.all()
    serializer_class = FournisseurSerializer

    def put(self, request, *args, **kwargs):
        try:
            fournisseur = Fournisseur.objects.get(id=kwargs['pk'])
        except Fournisseur.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        fournisseur_data = {
            'Nom_Complet': request.data['Nom_Complet'],
            'Telephone': request.data['Telephone'],
            'Adresse': request.data['Adresse'],
            'id_user': request.data['id_user'],
            'Date': request.data['Date'],
        }

        serializer = FournisseurSerializer(Fournisseur, data=fournisseur_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "errors": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully update",
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class FournisseurDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Fournisseur.objects.all()
    serializer_class = FournisseurSerializer

    def post(self, request, *args, **kwargs):
        try:
            Fournisseur = Fournisseur.objects.get(id=kwargs['pk'])
        except Fournisseur.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        Fournisseur.delete()
        return Response({
            "status": "succesc",
            "message": "depense successfully deleted"
        }, status=status.HTTP_200_OK)


class CommandeList(generics.CreateAPIView):
    serializer_class = CommandeSerializer

    def get(self, request, *args, **kwargs):
        commande = Commande.objects.filter(id_user=self.kwargs['pk'])

        if not commande:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = CommandeSerializer(commande, many=True)
        #print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "item successfully retrieved.",
            "count": commande.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = CommandeSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully create",
        }, status=status.HTTP_201_CREATED)


class CommandeUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Commande.objects.all()
    serializer_class = CommandeSerializer

    def put(self, request, *args, **kwargs):
        try:
            commande = Commande.objects.get(id=kwargs['pk'])
        except Commande.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        commande_data = {
            'Produit': request.data['Produit'],
            'Quantite': request.data['Quantite'],
            'Client': request.data['Client'],
            'id_user': request.data['id_user'],
            'Date': request.data['Date'],
        }

        serializer = CommandeSerializer(
            Commande, data=commande_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "errors": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully update",
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class CommandeDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Commande.objects.all()
    serializer_class = CommandeSerializer

    def post(self, request, *args, **kwargs):
        try:
            Commande = Commande.objects.get(id=kwargs['pk'])
        except Commande.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        Commande.delete()
        return Response({
            "status": "succesc",
            "message": "depense successfully deleted"
        }, status=status.HTTP_200_OK)


class BoutiqueList(generics.CreateAPIView):
    serializer_class = BoutiqueSerializer

    def get(self, request, *args, **kwargs):
        boutique = Boutique.objects.filter(id_user=self.kwargs['pk'])

        if not boutique:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = BoutiqueSerializer(boutique, many=True)
        #print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "item successfully retrieved.",
            "count": boutique.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = BoutiqueSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully create",
        }, status=status.HTTP_201_CREATED)


class BoutiqueUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Boutique.objects.all()
    serializer_class = BoutiqueSerializer

    def put(self, request, *args, **kwargs):
        try:
            boutique = Boutique.objects.get(id=kwargs['pk'])
        except Boutique.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        boutique_data = {
            'Nom_Gerant': request.data['Nom_Gerant'],
            'Nom_Boutique': request.data['Nom_Boutique'],
            'Telephone': request.data['Telephone'],
            'Email': request.data['Email'],
            'Adresse': request.data['Adresse'],
            'id_user': request.data['id_user'],
            'Date': request.data['Date'],
        }

        serializer = BoutiqueSerializer(
            boutique, data=boutique_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "errors": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully update",
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class BoutiqueDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Boutique.objects.all()
    serializer_class = BoutiqueSerializer

    def post(self, request, *args, **kwargs):
        try:
            Boutique = Boutique.objects.get(id=kwargs['pk'])
        except Boutique.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        Boutique.delete()
        return Response({
            "status": "succesc",
            "message": "depense successfully deleted"
        }, status=status.HTTP_200_OK)


class EmployeList(APIView):
    #permission_classes = (CustomDjangoModelPermissions, )
    serializer_class = EmployeSerializer

    def post(self, request, *args, **kwargs):
        serializer = EmployeSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully create",
        }, status=status.HTTP_201_CREATED)

    def get(self, request, *args, **kwargs):
        employe = Employe.objects.filter(user_emp_id=self.kwargs['pk'])

        if not employe:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)

        serializer = EmployeSerializer(employe, many=True)
        #print('quizs retrieves', serializer.data)

        return Response({
            "status": "success",
            "message": "item successfully retrieved.",
            "count": employe.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class LoginEmploye(generics.CreateAPIView):
    """
    POST auth/login/
    """

    # This permission class will over ride the global permission
    # class setting
    permission_classes = (permissions.AllowAny,)

    queryset = Employe.objects.all()
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        phone = request.data.get("phone", "")
        password = request.data.get("password", "")
        user = authenticate(request, phone=phone, password=password)
        if user is not None:
            # login saves the user’s ID in the session,
            # using Django’s session framework.
            login(request, user)

            serializer = TokenSerializer(data={
                # using drf jwt utility functions to generate a token
                "token": jwt_encode_handler(
                    jwt_payload_handler(user)
                )
            })
            if serializer.is_valid():
                user = request.user
                token = serializer.data
                response_data = {
                    'token': token,
                    'id': user.id,
                    'nom_complet': user.nom_complet
                }
                return Response(response_data)
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class EmployeUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Employe.objects.all()
    serializer_class = EmployeSerializer

    def put(self, request, *args, **kwargs):
        try:
            employe = Employe.objects.get(id=kwargs['pk'])
        except Employe.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        employe_data = {
            'nom_complet': request.data['nom_complet'],
            'phone': request.data['phone'],
            'email': request.data['email'],
            'adresse': request.data['adresse'],
            'salaire': request.data['salaire'],
        }

        serializer = EmployeSerializer(
            employe, data=employe_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "errors": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
            "status": "success",
            "message": "item successfully update",
            "data": serializer.data
        }, status=status.HTTP_200_OK)


class EmployeDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Employe.objects.all()
    serializer_class = EmployeSerializer

    def post(self, request, *args, **kwargs):
        try:
            Employe = Employe.objects.get(id=kwargs['pk'])
        except Employe.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)

        Employe.delete()
        return Response({
            "status": "succesc",
            "message": "depense successfully deleted"
        }, status=status.HTTP_200_OK)
