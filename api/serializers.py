from rest_auth.models import TokenModel
from rest_framework import serializers
from .models import User, Vente, Depense, Pret, Produit, Entree, Sortie, Historique, Client, Fournisseur
from api.models import Boutique, Commande, Employe


class UserSerializer(serializers.ModelSerializer):
    date_joined = serializers.ReadOnlyField()
    boutique = serializers.StringRelatedField(many=True)

    class Meta(object):
        model = User
        fields = ('id', 'phone', 'nom_complet',
                  'date_joined', 'password', 'boutique')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(
            phone=validated_data['phone'],
            nom_complet=validated_data['nom_complet']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class TokenSerializer(serializers.Serializer):
    """
    This serializer serializes the token data
    """
    token = serializers.CharField(max_length=255)


class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('phone', 'password')


class DepenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Depense
        fields = '__all__'


class VenteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vente
        fields = '__all__'


class PretSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pret
        fields = '__all__'


class ProduitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Produit
        fields = '__all__'


class EntreeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entree
        fields = '__all__'


class SortieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sortie
        fields = '__all__'


class HistoriqueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Historique
        fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):
    #vente = serializers.StringRelatedField(many=True)
    class Meta:
        model = Client
        fields = '__all__'
    

class FournisseurSerializer(serializers.ModelSerializer):
    #entree = serializers.StringRelatedField(many=True)
    class Meta:
        model = Fournisseur
        fields = '__all__'


class BoutiqueSerializer(serializers.ModelSerializer):
    #produit = serializers.StringRelatedField(many=True)
    class Meta:
        model = Boutique
        fields = '__all__'


class CommandeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Commande
        fields = '__all__'


class EmployeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Employe
        fields = ('nom_complet', 'phone', 'email', 'adresse','salaire', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = Employe(
            phone=validated_data['phone'],
            nom_complet=validated_data['nom_complet'],
            email=validated_data['email'],
            adresse=validated_data['adresse'],
            salaire=validated_data['salaire']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user
